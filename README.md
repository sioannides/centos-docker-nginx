centos-docker-nginx
========================

Docker image with Nginx and PHP-FPM

Developed by [Stelios Ioannides](https://stelios.ioannides.info).

### Documentation

Use relevant branch for relevant PHP version

Map local volume to /var/www/html and export port 8000 from container to access 
the served site:

```
docker run --rm -i -v src/webroot:/var/www/html -p 8000:8000 sioannides/centos-docker-nginx:latest
```

NOTE: nginx document_root is actually set to */var/www/html/webroot*

### License

[MIT](https://bitbucket.org/sioannides/centos-docker-nginx/src/master/LICENSE)

### Links

* Bitbucket: https://bitbucket.org/sioannides/centos-docker-nginx
